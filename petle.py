pack_mass = 0
number = 0
count_sent = 0
mass_sent = 0
emptyKg = 0
emptyKg_count = 0
full_sent = 0
emptyKg_packs = 0
max_emptyKg = 0
max_emptyKg_number = 0
mass = 1

print("\n")
print("*" * 30, "STSRT", "*" * 30)
print("\n")
max_number = input("Podaj maksymalna ilosc paczek do wysylki: ")
while not (max_number.isnumeric() and max_number != "0"):
    max_number = input("Podaj maksymalna ilosc paczek do wysylki: ")
max_number = int(max_number)
print(f"Podaj mase [kg] kazdego z {max_number} paczek do wyslania z przedzialu od 1[kg] do 10[kg]")
while number < max_number and mass != 0: # tu jak bedzie masa = 0 to zakonczy i wysle tak jakby osiagnal max_number!
    mass = float(input(f"Podaj mase massu nr {number + 1} do spakowania: "))
    if mass < 1 or mass > 10:
        print("\n")
        print("-" * 60)
        print("Error! mass dodawany ma więcej niż 10kg, lub mniej niż 1 kg")
        print("-" * 60)
        break
    pack_mass += mass  
    if pack_mass == 20:
        print("\n")
        print("--------------------------Wysyla pelna paczke 20[kg]-------------->")
        print("\n")
        count_sent += 1
        mass_sent += pack_mass
        pack_mass = 0
        number += 1
        continue
    elif pack_mass > 20:
        emptyKg_packs += 1
        print("\n")
        print(f"----------------Wysyla niepelna paczke nr {emptyKg_packs}, {round((pack_mass - mass), 2)}[kg]------------->")
        print("\n")
        count_sent += 1
        mass_sent += (pack_mass - mass)
        emptyKg = 20 - (pack_mass - mass)
        emptyKg_count += emptyKg
        if emptyKg > max_emptyKg:
            max_emptyKg = emptyKg
            max_emptyKg_number = emptyKg_packs
        pack_mass = mass
        number += 1
        continue
    else:
        number += 1
if pack_mass > 0 and mass >= 0 and mass <= 10: # spelniony dla masa = 0!
    emptyKg_packs += 1
    print("\n")
    print(f"----------------Wysyla niepelna paczke nr {emptyKg_packs}, {round(pack_mass, 2)}[kg]------------->")
    count_sent += 1
    mass_sent += pack_mass
    emptyKg = 20 - pack_mass
    emptyKg_count += emptyKg
    if emptyKg > max_emptyKg:
        max_emptyKg = emptyKg
        max_emptyKg_number = emptyKg_packs
if mass >= 0 and mass <= 10: # spelniony dla masa = 0!
    print("\n")
    print("Liczba paczek wyslanych: ", count_sent)
    print("Liczba kilogramow wyslanych: ", round(mass_sent, 2))
    if emptyKg_count != 0:
        print("Suma 'pustych' - kilogramów (brak optymalnego pakowania): ", round(emptyKg_count, 2))
 != 0:
        print("Liczba paczek * 20 - liczba kilogramów wysłanych:)
    if max_emptyKg_number != 0:
        print(f"Niepelna paczka numer {max_emptyKg_number} miala najwiecej - {round(max_emptyKg, 2)} pustych kilogramow ")
print("\n")
print("*" * 30, "END", "*" * 30)
print("\n")